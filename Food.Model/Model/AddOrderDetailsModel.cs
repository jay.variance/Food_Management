﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food.Model.Model
{
    public class AddOrderDetailsModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage ="ItemName is required")]
        public int ItemName { get; set; }
        [Required(ErrorMessage = "Quantity is required")]
        public int Quantity { get; set; }
        public int Price { get; set; }
        public DateTime PrepareTime { get; set; }

        [Required(ErrorMessage = "PaymentType is required")]
        public int PaymentType { get; set; }
    }
}
