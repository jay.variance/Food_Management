﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food.Model.Model
{
    public class UpdateOrderDetailsModel
    {
        public long ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public int ItemName { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public DateTime PrepareTime { get; set; }
        public int PaymentType { get; set; }
    }
}
