﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food.Model.Model
{
    public class DisplayOrderDetailsByIdModel
    {
        public long ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PaymentType { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public long FoodId { get; set; }
        public TimeSpan PrepareTime { get; set; }
        public string Phone { get; set; }
    }
}
