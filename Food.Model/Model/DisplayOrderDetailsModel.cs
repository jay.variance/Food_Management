﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food.Model.Model
{
    public class DisplayOrderDetailsModel
    {
        public long ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PaymentType { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public string ItemName { get; set; }
        public TimeSpan PrepareTime { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
