﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food.Model.Model
{
    public class FoodModel
    {
        public long FoodId { get; set; }
        public string ItemName { get; set; }
        public int Price { get; set; }
        public System.DateTime CreatedDate { get; set; }
    }
}
