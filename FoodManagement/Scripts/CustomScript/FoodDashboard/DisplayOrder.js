﻿var price;
var price1;
var itemInformation;

$(document).ready(function () {
    loadOrderDetails();
    $("#ItemName").change(function () {
        if ($("#ItemName").val() > 0) {
            $("#Quantity").val("");
            $("#Price").val("");
            $.get("/Order/ItemPrice", { FoodId: $("#ItemName").val() }, function (data) {
                $("#Price").val(data.Price);
                price = data.Price;
            });
        } else {
            $("#Quantity").val("");
            $("#Price").val("");
        }
    });

    $("#Quantity").change(function () {
        if ($("#Quantity").val() > 0 && $("#Quantity").val() <= 15) {
            if ($("#ItemName").val() != null && $("#ItemName").val() != "0") {
                var quantity = $("#Quantity").val();
                var MainPrice;
                var finalprice = quantity * price;
                if (price1 != undefined) {
                    $.each(itemInformation, function (key, value) {
                        if (value.FoodId == price1.FoodId) {
                            MainPrice = value.Price;
                        }
                    });
                    var finalprice = quantity * MainPrice;
                }
                $("#Price").val("");
                $("#Price").val(finalprice);
            }
        }
        else {
            $("#Price").val("");
        }
    });
 
    $("#myModel").click(function () {
        $("#FirstName").val("ClientId");
        $("#FirstName").val("");
        $("#LastName").val("");
        $("#PhoneNumber").val("");
        $("#ItemName").val("0");
        $("#Quantity").val("");
        $("#Price").val("");
        $("#PrepareTime").val("");
        $("#PaymentType").val("");
        $('#btnUpdate').hide();
        $('#btnAdd').show();
        $("#myModal").modal('show');
    });

    $("#formOrder").validate({
        errorClass: 'tst',
        rules: {

            FirstName: {
                required: true
            },
            LastName: {
                required: true
            },
            PhoneNumber: {
                required: true, 
                minlength: 10,
                maxlength: 12
            },
            ItemName: {
                required: true,
                min: 1
            },
            Quantity: {
                required: true,
                min: 1,
                max: 15
            },
            Price: {
                required: true
            },
            PrepareTime: {
                required: true
            },
            PaymentType: {
                required: true
            },
        },

        messages: {

            FirstName: {
                required: "Please enter first name"
            },
            LastName: {
                required: "Please enter last name"
            },
            PhoneNumber: {
                required: "Please enter phone number"
            },
            ItemName: {
                min: "Please select itemname"
            },
            Quantity: {
                required: "Please enter quantity",
                min: "Please Enter quantity between 1 to 15",
                max: "Please Enter quantity between 1 to 15"
            },
            Price: {
                required: "Please select price and enter quantity"
            },
            PrepareTime: {
                required: "Please enter preparetime"
            },
            PaymentType: {
                required: "Please select paymenttype"
            }
        },
    });

    var timeOptions = {
        'timeFormat': 'h:i A',
        'minTime': getCurrentTime(new Date())
    };
    $("#PrepareTime").timepicker(timeOptions);
    $.ajax({
        type: "POST",
        url: "/Order/LoadItem",
        data: '{}',
        success: function (data) {
            itemInformation = data;
            $("#ItemName").empty();
            $("#ItemName").append('<option value="0"> -- Select Item -- </option>');
            $.each(data, function (index, row) {                
                $("#ItemName").append("<option value='" + row.FoodId + "'>" + row.ItemName + "</option>")
            });
        },
        error: function (data) {
            alert(data.responseText);
        }
    });
});

function loadOrderDetails() {
    $.ajax({
        url: "/FoodDashboard/getOrderDetails",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadedPrice = result;
            var html = '';
            var totalQantity = 0;
            var totalPrice = 0;
            var preparationTime = '';
            $.each(result, function (key, item) {
                if (item.PrepareTime.Minutes == 0) {
                    preparationTime = "Item deliver";
                } else {
                    preparationTime = item.PrepareTime.Minutes;
                }
                html += '<tr><td>' + item.ClientId + '</td>'
                    + '<td>' + item.FirstName + '</td>' 
                    + '<td>' + item.ItemName + '</td>'
                    + '<td>' + item.Quantity + '</td>'
                    + '<td>' + '$' + item.Price + '</td>'
                    + '<td>' + preparationTime + '</td>'
                    + '<td>' + item.PaymentType + '</td>'
                    + '<td><a href="#" onclick="return getByClientId(' + item.ClientId + ')">Edit</a> | <a href="#" onclick="deleteOrder(' + item.ClientId + ')">Delete</a></td>'
                    + '</tr>';
                totalPrice += item.Price;
                totalQantity += item.Quantity;
            });  
            $('.tbody').html(html);
            $('#totalquantity').html(totalQantity);
            $('#totalprice').html("$"+ totalPrice);
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

function getByClientId(Id) {
    $.ajax({
        url: "/FoodDashboard/getOrderDetailsByClientId/" + Id,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            price1 = result;
            $("#ClientId").val(result.ClientId);
            $("#FirstName").val(result.FirstName);
            $("#LastName").val(result.LastName);
            $("#PhoneNumber").val(result.Phone);           
            $("#ItemName").val(result.FoodId);
            $("#Quantity").val(result.Quantity);
            $("#Price").val(result.Price);
            $("#PrepareTime").val(result.PrepareTime.Hours + ":" + result.PrepareTime.Minutes);
            $("#PaymentType").val(result.PaymentType);
            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

function UpdateOrderDetails() {
    if ($("#formOrder").valid()) {
        var updatedOrderDetails = {
            ClientId: $("#ClientId").val(),
            FirstName: $("#FirstName").val(),
            LastName: $("#LastName").val(),
            PhoneNumber: $("#PhoneNumber").val(),
            ItemName: $("#ItemName").val(),
            Quantity: $("#Quantity").val(),
            Price: $("#Price").val(),
            PrepareTime: $("#PrepareTime").val(),
            PaymentType: $("#PaymentType").val()
        };
        $.ajax({
            url: "/FoodDashboard/UpdateOrderDetails",
            data: JSON.stringify(updatedOrderDetails),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                $('#myModal').modal('hide');
                loadOrderDetails();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}

function deleteOrder(Id) {
    var ans = confirm("Are you sure you want to delete this Record?");
    if (ans) {
        $.ajax({
            url: "/FoodDashboard/deleteOrder/" + Id,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                loadOrderDetails();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}

function AddOrder() {
    if ($("#formOrder").valid()) {
        var orderDetails = {
            FirstName: $("#FirstName").val(),
            LastName: $("#LastName").val(),
            PhoneNumber: $("#PhoneNumber").val(),
            ItemName: $("#ItemName").val(),
            Quantity: $("#Quantity").val(),
            Price: $("#Price").val(),
            PrepareTime: $("#PrepareTime").val(),
            PaymentType: $("#PaymentType").val()
        };
        $.ajax({
            url: "/Order/AddOrder",
            data: JSON.stringify(orderDetails),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response == false) {
                    alert("Invalid phone number.");
                }
                else {
                    $('#myModal').modal('hide');
                    loadOrderDetails();
                }
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }
}

function getCurrentTime(date) {
    var hours = date.getHours(),
        minutes = date.getMinutes(),
        ampm = hours >= 12 ? 'pm' : 'am';

    hours = hours % 12;
    hours = hours ? hours : 12; 
    minutes = minutes < 10 ? '0' + minutes : minutes;

    return hours + ':' + minutes + ' ' + ampm;
}

function refreshModel() { 
    location.reload(true);    
}
