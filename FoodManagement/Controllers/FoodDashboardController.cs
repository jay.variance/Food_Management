﻿using Food.Model.Model;
using FoodManagement.Models;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace FoodManagement.Controllers
{
    public class FoodDashboardController : Controller
    {

        FoodManagementEntities foodManagementEntities = new FoodManagementEntities();

        public ActionResult DisplayOrder()
        {
            return View();
        }

        public JsonResult getOrderDetails()
        {
            var orderDetails = foodManagementEntities.Database.SqlQuery<DisplayOrderDetailsModel>("getOrderDetails").ToList();

            foreach(var x in orderDetails)
            {
                if(x.CreatedDate.Day == DateTime.Now.Day && x.PrepareTime > DateTime.Now.TimeOfDay)
                {
                    x.PrepareTime = x.PrepareTime - DateTime.Now.TimeOfDay;                    
                }
                else
                {
                    TimeSpan setdefaultTime = new TimeSpan(00, 00, 00);
                    x.PrepareTime = setdefaultTime;
                }
            }
            return Json(orderDetails.Take(5), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getOrderDetailsByClientId(long Id)
        {
            SqlParameter param = new SqlParameter("@ClientId", Convert.ToInt64(Id));
            var orderDetails = foodManagementEntities.Database.SqlQuery<DisplayOrderDetailsByIdModel>("getOrderDetailsByClientId @ClientId", param).SingleOrDefault();
            return Json(orderDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateOrderDetails(UpdateOrderDetailsModel updateOrderDetailsModel)
        {
            try
            {
                SqlParameter clientid = new SqlParameter("@ClientId", updateOrderDetailsModel.ClientId);
                SqlParameter firstname = new SqlParameter("@FirstName", updateOrderDetailsModel.FirstName);
                SqlParameter lastname = new SqlParameter("@LastName", updateOrderDetailsModel.LastName);
                SqlParameter phonenumber = new SqlParameter("@PhoneNumber", updateOrderDetailsModel.PhoneNumber);
                SqlParameter itemname = new SqlParameter("@ItemName", updateOrderDetailsModel.ItemName);
                SqlParameter quantity = new SqlParameter("@Quantity", updateOrderDetailsModel.Quantity);
                SqlParameter price = new SqlParameter("@Price", updateOrderDetailsModel.Price);
                SqlParameter paymenttype = new SqlParameter("@PaymentType", updateOrderDetailsModel.PaymentType);
                SqlParameter preparetime = new SqlParameter("@PrepareTime", updateOrderDetailsModel.PrepareTime.TimeOfDay);

                var result = foodManagementEntities.Database.ExecuteSqlCommand(@"exec UpdateOrderDetails @ClientId, @FirstName, @LastName, @PhoneNumber, @ItemName, @Quantity, @Price, @PaymentType, @PrepareTime ",
                  clientid, firstname, lastname, phonenumber, itemname, quantity, price, paymenttype, preparetime);
                return Json("Order successfully added");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        public JsonResult deleteOrder(long Id)
        {
            try
            {
                SqlParameter clientid = new SqlParameter("@ClientId", Id);

                var result = foodManagementEntities.Database.ExecuteSqlCommand(@"exec deleteOrder @ClientId", clientid);
                return Json("Order successfully added");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}