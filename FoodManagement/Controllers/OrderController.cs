﻿//using Food.BusinessLogic.IService;
using Food.Model.Model;
using FoodManagement.Models;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace FoodManagement.Controllers
{
    public class OrderController : Controller
    {
        FoodManagementEntities foodManagementEntities = new FoodManagementEntities();

        [HttpPost]
        public JsonResult AddOrder(AddOrderDetailsModel addOrderDetailsModel)
        {
            try
            {
                SqlParameter firstname = new SqlParameter("@FirstName", addOrderDetailsModel.FirstName);
                SqlParameter lastname = new SqlParameter("@LastName", addOrderDetailsModel.LastName);
                SqlParameter phonenumber = new SqlParameter("@PhoneNumber", addOrderDetailsModel.PhoneNumber);
                SqlParameter itemname = new SqlParameter("@ItemName", addOrderDetailsModel.ItemName);
                SqlParameter quantity = new SqlParameter("@Quantity", addOrderDetailsModel.Quantity);
                SqlParameter price = new SqlParameter("@Price", addOrderDetailsModel.Price);
                SqlParameter paymenttype = new SqlParameter("@PaymentType", addOrderDetailsModel.PaymentType);
                SqlParameter preparetime = new SqlParameter("@PrepareTime", addOrderDetailsModel.PrepareTime.TimeOfDay);

                var result = foodManagementEntities.Database.ExecuteSqlCommand(@"exec InsertOrderDetails @FirstName, @LastName, @PhoneNumber, @ItemName, @Quantity, @Price, @PaymentType, @PrepareTime ",
                              firstname, lastname, phonenumber, itemname, quantity, price, paymenttype, preparetime);
                if (result == -1)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                return Json("Order successfully added");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult LoadItem()
        {
            var data = foodManagementEntities.Database.SqlQuery<FoodModel>("getItems").ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ItemPrice(long FoodId)
        {
            SqlParameter param = new SqlParameter("@FoodId", Convert.ToInt64(FoodId));
            var x = foodManagementEntities.Database.SqlQuery<FoodModel>("getItemPriceByFoodId @FoodId", param).SingleOrDefault();
            return Json(x, JsonRequestBehavior.AllowGet);
        }
    }
}